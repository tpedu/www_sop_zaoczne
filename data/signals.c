#include <stdio.h>
#include <signal.h>
volatile sig_atomic_t licznik = 0;
void handler (int signum) {
	printf("dostalem sygnal..\n");
	licznik++;
}

int main (void) {
  signal (SIGINT, handler);
  printf("Ctrl+C Nic Ci nie da  (%d):-P\n",getpid());
  while (licznik < 10) sleep(1);
  printf("Sam się zakończę\n");
  return 0;
}
