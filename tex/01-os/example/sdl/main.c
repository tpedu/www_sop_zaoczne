#include <SDL.h>
// gcc `pkg-config --cflags sdl2` main.c  `pkg-config --libs sdl2`

int main(int argc, char **argv) {
	SDL_Window* window;
	SDL_Renderer* renderer;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) return 1;
	window = SDL_CreateWindow("Przykladowe okienko",
					SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
					512, 512, 0);
	renderer = SDL_CreateRenderer(window, -1, 0);
	SDL_Surface* gHelloWorld = SDL_LoadBMP( "hello.bmp" );
	SDL_Texture* bunny = SDL_CreateTextureFromSurface(renderer, gHelloWorld);
	SDL_SetRenderDrawColor(renderer, 0, 10, 0, 255);
	SDL_RenderCopy(renderer, bunny, NULL, NULL);
	SDL_RenderPresent(renderer);
	SDL_Delay(5000);
	SDL_DestroyTexture(bunny);
	SDL_Quit();
	return 0;
}
