#!/bin/bash -e

for i in `seq 1 8`
do
echo "# Laboratorium $i" > lab$i.md
echo "" >> lab$i.md
echo "## Materiały" >> lab$i.md
echo "" >> lab$i.md
echo "* [wykład $i](tex/$i/sop_$i.pdf)" >> lab$i.md
echo "* todo" >> lab$i.md
echo "" >> lab$i.md
echo "## Zadanie dzisiejsze" >> lab$i.md
echo "" >> lab$i.md
echo "## Zadanie domowe" >> lab$i.md
echo "" >> lab$i.md
done
