# 03 Bash 2

Dzisiejsze zajęcia trochę większe, zachęcam do dokończenia w domu.

Plan na dziś to: Regex, tablice, for, while, if, case, funkcje, wątki (wait)

* [bash](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html)

## Bash - wyrażenia warunkowe

Wyrażenia warunkowe w Bash robi się tak:

```bash
if warunek
then
 ....
elif warunek
then
 ...
else
 ...
fi
```

Można oczywiście krócej:

```bash
if warunek; then
 ....
fi
```

Warunki można przedstawiać tradycyjnie w nawiasach kwadratowych:

```bash
if [[ "abc" != "$1" ]]; then
  echo "parametr to nie abc"
fi
```

Polecam zapoznać się z dokumentacją - [link do dokumentacji](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Bash-Conditional-Expressions-1)

### Ćwiczenia 1

1. Napisz skrypt, który sprawdzi, czy podano więcej niż jeden parametr. Jeśli tak, to niech wyświetli pierwszy parametr. Jeśli nie, to niech wyświetli komunikat, że nie podano parametrów.
1. Napisz skrypt, który sprawdzi, czy istnieje plik ```config.cfg``` w bieżącym katalogu. Jeśli taki plik istnieje, to niech go wykona. Jeśli nie istnieje, to niech sprawdzi, czy podano argument i wykona plik o nazwie takiej jak wartość argumentu (parametru) skryptu. Jeśli żadne z powyższych nie nastąpiło, to niech wyświetli komunikat o błędzie.
1. Napisz skrypt, który sprawdzi, czy jego nazwa kończy się ```.sh```. Jeśli nie, to niech zmieni swoją nazwę poprzez dopisanie tego rozszerzenia ([rozszerzenie nazwy pliku](https://pl.wikipedia.org/wiki/Rozszerzenie_nazwy_pliku)). Sprawdzenie można zrobić na kilka sposobów, polecam przetestować 2 typowe:
   * dopasowanie do wyrażenia regularnego (to jest bardziej uniwersalny sposób)
   * porównanie ostatnich 3 liter nazwy skryptu
1. Napisz skrypt, który sprawdzi, czy w bieżącym katalogu jest więcej niż 5 plików. Jeśli tak, to wypisze odpowiedni komunikat z informacją że tak jest. Podpowiem:
   * ls - wyświetla listę plików
   * wc - word count - zlicza znaki, słowa i linie

## Pętle

Bash obsługuje pętle for, while i until. Zobacz [pętle w dokumentacji](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Looping-Constructs). Prowadzący pokaże wykorzystanie pętli. Podpowiedzi są takie:

```bash
for i in a b c 1 2 3; do
echo $i
done

for ((i = 1; i < 10; i++)); do echo $i; done

while [ true ]; do
  echo "to się nigdy nie skończy..."
  sleep 1
done
```

## Tablice

Bash obsługuje tablice. Wyglądają one jak zwykłe zmienne, ale nie można ich w zwykły sposób eksportować. Zobacz [dokumentacja -- tablice](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Arrays).

Deklaracja i użycie (przetestuj, lub zerknij do dokumentacji):

```bash
TABLICA=(element1 "element 2" element\ 3)
echo ${TABLICA[1]}
echo ${TABLICA[*]}
echo ${TABLICA[@]}
echo ${TABLICA[*]}
echo "${!TABLICA[@]}  ${!TABLICA[*]}"
echo ${#TABLICA[@]}
```

## Ćwiczenia 2

1. Napisz skrypt, który będzie wypisywał liczby od 10 do 1 co 2
1. Napisz skrypt, który będzie wypisywał wszystkie argumenty wywołania skryptu (pamiętaj o tym, że argument może w sobie zawierać jakieś spacje) napisane w cudzysłowach.
1. Napisz skrypt, który wypisze listę plików i katalogów bieżącego katalogu poprzedzając każdą nazwę pliku tekstem "Patrzcie Państwo, oto plik: ". Podpowiedź [internal field separator](https://en.wikipedia.org/wiki/Internal_field_separator)
1. Napisz skrypt, który dla każdego pliku w bieżącym katalogu, który ma rozszerzenie .c wykona taką komendę:
   * ```cc (tunazwapliku) -o (tunazwaplikubezrozszerzenia)```
   * Oczywiście zamień (tunazwapliku) na nazwę pliku, a (tunazwaplikubezrozszerzenia) na nazwę pliku z obciętym rozszerzeniem. Jeśli chcesz przykładowe pliki do potestowania, [oto one:-)](http://lmgtfy.com/?q=example+c+file)
1. Napisz skrypt, który wczyta listę plików do wypisania z wiersza poleceń, a następnie wypisze ich zawartości raz w kolejności od początku do końca, a raz na odwrót. Podpowiedzi:
   * indeksy ujemne tablicy mogą się przydać
   * można wyliczać wartości wyrażeń arytmetycznych ```(i+1)```
   * przyda się for

## Funkcje

Bash obsługuje funkcje. Funkcję deklaruje się tak:

```bash
function witaj {
  echo "Funkcja ta jest nieuprzejma i nie wita $1"
  # tu opcjonalnie: return 0
}
```

Argumenty są liczone tak jak przy zwykłym skrypcie (```$1, $2, ...```).

Funkcję wywołuje się po prostu przez nazwę.

Jeśli chcesz zaimportować plik z funkcjami, to można to zrobić tak:

```bash
. ./plikDoZaimportowania
```

Tak właściwie, to taki plik zostanie dołączony w tym miejscu do bieżącego skryptu.

## Ćwiczenia 3

1. Przygotuj skrypt z funkcją która będzie wypisywała tekst pokolorowany na zadany kolor. Niech funkcja ta przyjmuje dwa argumenty - tekst i kolor. Zobacz ```man console_codes``` oraz składnię komendy echo (lub przykład z poprzednich zajęć).
1. Przygotuj funkcję obliczającą rekurencyjnie ciąg Fibonacciego. Niech wynik zwraca za pomocą return. Czy są jakieś ograniczenia?
1. Dla ambitnych: Napisz swoją funkcję do autouzupełniania komend. Tu można puścić wodze fantazji :).  Zobacz [funkcja do uzupełniania tekstu](https://www.gnu.org/software/bash/manual/html_node/Programmable-Completion.html)

## Zadanie domowe

Proszę wysłać rozwiązania na email do prowadzącego (dotyczy grupy TP, w innych grupach prowadzący może zaproponować inną metodę oddania zadania). W przypadku bardzo podobnych rozwiazań nie będę ich uznawał (TP).

1. Przygotuj wyrażenie regularne, które będzie sprawdzało, czy tekst jest poprawnie napisanym polskim imieniem (od wielkiej litery, może mieć tylko litery i zawierać polskie znaki).
1. Przygotuj wyrażenie regularne sprawdzające czy tekst jest kodem pocztowym (cały tekst, czyli zaczyna się od cyfry i kończy się cyfrą).
1. Przygotuj wyrażenie regularne sprawdzające e-mail.

Napisz skrypt który będzie pobierał jeden argument z linii komend i wypisywał informację o tym, czy jest to imie, kod pocztowy, czy też email. Na przykład

```bash
./coto Janusz
imię
./coto 68-221
kod pocztowy
./coto 'muniek+spam@gmail.com'
email
```
