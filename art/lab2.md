# 02 Bash 2

Bash jest to najprawdopodobniej najpopularniejsza [powłoka systemowa](https://pl.wikipedia.org/wiki/Pow%C5%82oka_systemowa) oraz [język skryptowy](https://pl.wikipedia.org/wiki/J%C4%99zyk_skryptowy) dla [systemów](https://pl.wikipedia.org/wiki/System_operacyjny) z rodziny Linux. Pozwala na bardzo zaawansowane korzystanie z komputera w trybie tekstowym.

## Skrypty

Skrypt powłoki jest to plik, zawierający kolejne komendy do zinterpretowania przez powłokę systemową. Są to pliki tekstowe. W Windows mamy najczęściej pliki .bat (typowo powłokowe) oraz pliki .vbs (VisualBasic Script), pod Linuksem i w większości systemów Unix-owych jest .sh (od shell), chociaż akurat rozszerzenie jest kwestią umowną.

Skupimy się na powłoce Linuksa, a konkretnie na [bash-u](https://www.gnu.org/software/bash/).

Skrypty w systemie Linux powinny mieć atrybut "wykonywalny". Jeśli w pliku nie jest określone, z jakiego interpretera należy skorzystać, wtedy skrypt uruchamia się w domyślnej powłoce systemu.

Skrypty w systemie Linux mogą zawierać informację o tym, jaki jest program interpretujący dany skrypt, robi się to tak, że na początku (pierwsza linia) umieszcza się znak hash, wykrzyknik, a następnie nazwę programu interpretera. U nas będzie to:

```bash
#!/bin/bash
```

Co oznacza, że program interpretujący ten skrypt to będzie właśnie bash.

Znak hash (#) oznacza komentarz, tak więc można zrobić mały skrypt (proponuję stworzyć taki) o takiej treści:

```bash
#!/bin/bash

# Skrypt wykorzystuje zaawansowane funkcje 
# systemu do tego, aby narysować króliczka:
echo -e "    \e[m(\\_/)"
echo -e "   (=\e[33m'\e[31m.\e[33m'\e[0m=)"
```

### Ćwiczenie 1

Napisz skrypt wyświetlający na terminalu tekst "Witamy w terminalu". Skorzystaj z komendy ```echo``` oraz informacji podanych powyżej. Pamiętaj o atrybutach i o tym jak się wywołuje programy. Prowadzący podpowie.

## Zmienne powłoki

Powłoka systemowa udostępnia metodę na przechowywanie par "nazwa:wartość" za pomocą tak zwanych zmiennych środowiskowych i zmiennych programowych (lub lokalnych).

Stworzenie nowej zmiennej programowej (będę nazywał to po prostu zmienną, albo zmienną lokalną) robi się tak:

```bash
ZMIENNA=wartość
```

Działa to na poziomie wiersza poleceń i w skryptach.

* Wartość może być podana po prostu, ale wtedy musi to być jedno słowo
* albo w cudzysłowach ("), wtedy może być to kilka słów i znaków, natomiast znaki specjalne są interpretowane, a zmienne tłumaczone na wartości
* albo w apostrofach  ('), wtedy można przekazać dowolny ciąg znaków nie zawierający apostrofów

Zmienne lokalne (po prostu zmienne) są widoczne tylko w danej instancji powłoki (aktualnie wykonywany skrypt, lub aktualna powłoka), natomiast zmienne środowiskowe są dostępne także w powłokach i programach wykonywanych w danej powłoce (takie dziedziczenie zmiennych). Aby zmienna była widoczna w podpowłokach wystarczy wydać komendę export, na przykład:

```bash
export ZMIENNA=wartość
```

albo

```bash
ZMIENNA=wartość
export ZMIENNA
```

Usunięcie zmiennej środowiskowej robi się tak:

```bash
export -n
```

Aby odczytać zawartość zmiennej można się do niej odwołać w taki sposób:

```bash
$ZMIENNA
${ZMIENNA}
```

Ta druga składnia jest konieczna, kiedy zmienna ma w nazwie cyfry, albo jest to tablica. Ewentualnie kiedy chcemy wykonać konkatenację (o tym za chwilę).

### Ćwiczenie 2

Zobacz taką serię poleceń:

```bash
X=tekst
echo $X
bash
echo $X
```

Co się stało. Czy potrafisz wyjaśnić?

Zobacz:

```bash
X=Tekst dłuższy
echo $X
```

Co jest nie tak? jak to naprawić? Zobacz czy pomoże zastosowanie cudzysłowów i apostrofów.

Zobacz:

```bash
X="Tekst 1"
Y="tekst:$X"
echo "$Y"
Y='tekst:$X'
echo "$Y"
Y=tekst:$X
echo "$Y"
```

Jaka jest różnica między " a ' ?

```bash
A=Ala
echo $A ma kota, a kot ma ${A}ę
```

Tak, tu coś będzie nie tak, zaraz to naprawimy.

## Zmienne'

Domyślnie istnieje kilka predefiniowanych zmiennych środowiskowych, które przechowują pewne użyteczne informacje. Przetestujemy sobie je jako ćwicznie. Ponad to są zmienne które są związane ze sposobem uruchomienia skryptu:

* ```$*```, ```$@``` - wszystkie argumenty skryptu. Różnicę pokażę za jakiś czas, albo na rzutniku. Jeśli ktoś wie już teraz, to proszę się zgłosić i wyjaśnić - dam plusa.
* ```$0``` - nazwa skryptu
* ```$1```, ```$2```, ... - kolejne argumenty skryptu
* ```$?``` - kod zakończenia ostatniego polecenia
* ```$$``` - PID procesu bieżącej powłoki

### Ćwiczenie 3

Zobacz co przechowują zmienne:

```bash
PATH
RANDOM
PWD
PS1
USER
HOSTNAME
OSTYPE
```

## Zmienne''

Pod wartość zmiennej można przypisać standardowe wyjście jakiejś komendy. Robi się to na dwa sposoby. Bezpieczniejszy, ale dłuższy, oraz krótszy, ale w niektórych sytuacjach nie zadziała:

```bash
ZM=$(ls -la)
ZM=`ls -la`
```

Pierwszy sposób jest bezpieczniejszy - zawsze działa, ale wymaga 3 różnych znaków, natomiast drugi jest bardziej przejrzysty. Oba w tym przykładzie dadzą taki sam efekt.

### Ćwiczenie 4

Wykonaj komendę ```ls -l``` w podpowłoce i przypisz ją do zmiennej ```X```.

Wyświetl zawartość tej zmiennej.

Spraw, aby nie było różnicy (co najwyżej kolorki) między zwykłym wykonaniem komendy:

```bash
ls -l
```

a wypisaniem zawartości zmiennej ```X```. Nie będzie dobrego rezultatu gdy zrobimy tak:

```bash
echo $X
```

Zastanów się dlaczego.

## Operacje na ciągach znaków

Podczas odwoływania się do zmiennych, możemy wykonywać proste operacje związane z ciągami znaków, oraz można wykonywać operacje arytmetyczne. Zobacz:

```bash
echo $((2+2))
X=5
echo $((2*X))
```

Operacje na ciągach znaków (zobacz też [tu](http://www.thegeekstuff.com/2010/07/bash-string-manipulation/):

```bash
${#TEKST} - długość ciągu znaków
${TEKST:p} - podciąg rozpoczynający się od znaku o indeksie p
${TEKST:p:l} - podciąg rozpoczynający się od znaku o indeksie p, a długości l
${TEKST/w/z} - zamienia tekst pasujący do ```w``` na tekst ```z```
```

### Ćwiczenie 5

Pamiętasz jedno z zadań poprzednich?

```bash
A=Ala
echo $A ma kota, a kot ma ${A}ę
```

Przerób je tak, aby tekst wypisywał się poprawnie.

Napisz skrypt, który będzie wypisywał taki tekst także dla imion męskich. Pomijamy zdrobnienia i imiona które się dziwnie odmieniają (na przykład Marek). Niech skrypt ten przyjmuje jeden argument - imię. Przykładowe wywołanie:

```bash
 $ ./pszetżkole Ala
Ala ma kota, a kot ma Alę
 $ ./pszetżkole Justyna
Justyna ma kota, a kot ma Justynę
 $ ./pszetżkole Stefan
Stefan ma kota, a kot ma Stefana
```

Czy jest to możliwe w czystym bash-u? Za chwilę na to odpowiemy.

Napisz skrypt, który wczyta jako argument jakiś tekst. Skrypt ten wypisze kolejno:

1. Pierwszy znak z argumentu
1. Ostatni znak z argumentu
1. Zamieni w argumencie tekst SOP na tekst poniżej (zastosuj echo z przełącznikiem -e):

```bash
\\e[32mSOP\\e[0m
```

## Wyrażenia regularne

Bash jako taki obsługuje w niektórych aspektach wyrażenia regularne, ale niestety nie jest to tak wygodne, jakby się czasami marzyło. Na szczęście jest program sed.

Program sed jest określany przez twórców jako edytor tekstowy bez interfejsu użytkownika. Coś w tym jest. My nie będziemy się zgłębiać w całą składnie polecenia sed, natomiast skupimy się na małym wycinku - przetwarzaniu [wyrażeń regularnych](https://pl.wikipedia.org/wiki/Wyra%C5%BCenie_regularne).

Sed najczęściej przetwarza ciąg znaków który dociera do niego przez standardowe wejście i zwraca wynik na standardowe wyjście. Składnia wyrażeń regularnych jest w większości zgodna z innymi programami. Podstawowe elementy składni wyrażeń wyglądają tak:

* ```\[…\]``` - w miejsce … wchodzi lista znaków które mogą wystąpić na danym miejscu
* ```()``` - zbiór opcji
* ```^``` - początek ciągu znaków
* ```$``` - koniec ciągu znaków
* ```^``` - negacja
* ```*``` - wzorzec ma się powtarzać dowolną liczbę razy (od 0 do nieskończoności)
* ```+``` - wzorzec ma się powtarzać co najmniej raz
* ```.``` - dowolny znak
* ```…-…``` - zakres znaków
* ```\``` - znak modyfikacji (escape character)

Przykład zamiany (opowiem na zajęciach)

```bash
echo Alicja | sed s/a$/ę/g | sed 's/\([^ę]\)$/\1a/g'
```

### Ćwiczenie 6

Teraz postaraj się zrobić to zadanie z haczykiem i podmianą fragmentu zmiennej (Ala ma ...)


## Zadanie domowe

Przygotuj skrypt, który odczyta plik (jego nazwa ma być argumentem skryptu) i wypisze go na terminalu tak, aby wszystkie wystąpienia Twojego imienia były podświetlone na czerwono.
