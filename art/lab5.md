# 05 Procesy 1

* fork
* wait
* getpid
* getppid

zobacz też

```bash
ps auxf
```

Dodatkowe materiały od T. Idzikowskiego: [O komendzie fork](data/procesy_tidzikowski.pdf)

## Zadania

1. Napisz program, który przedstawi swój numer PID, oraz PPID.
1. Niech program z poprzedniego zadania będzie dodatkowo, po wypisaniu, czekał na standardowym wejściu na dane od użytkownika.
1. Uruchom program z poprzedniego zadania - nic nie podawaj na wejściu (program powinien czekać w nieskończoność). Za pomocą komendy kill zamknij ten proces korzystając z PID wypisanego przez niego.
1. Napisz program, który uruchomi podproces za pomocą komendy fork. Niech proces rodzica oczekuje na proces potomka. Oba procesy powinny wypisać swój PID oraz PPID.
1. Napisz program, który będzie obliczał w 2 procesach:
    * Proces rodzica: Wartość sumy liczb od 0 do wartości zadanej jako pierwszy argument wykonania programu.
    * Proces potomny: Wypisujący liczby nieparzyste od 1 do  wartości zadanej jako pierwszy argument wykonania programu.
      Program powinien wypisać wyniki w kolejności takiej, że najpierw wypisze wynik proces potomny, a następnie proces rodzica. Nie korzystaj z komendy sleep.
1. Napisz program, który uruchomi 100 procesów w taki sposób, że każdy kolejny potomek stworzy potomka. Niech każdy proces poczeka na zakończenie procesu potomka. Możesz korzystać z oczekiwania na wejście lub z komendy sleep i zobacz czy drzewko procesów jest takie jak się spodziewasz (możesz w tym celu ograniczyć liczbę procesów do 10). Zobacz podpowiedź na początku tej strony.
1. Napisz program, który uruchomi 100 procesów potomnych w taki sposób, że będzie jeden rodzic i 100 potomków. Proces główny niech oczekuje na zakończenie wszystkich procesów potomnych.
1. Napisz prosty program chat-u, który będzie korzystał z pliku w katalogu /tmp do przekazywania wiadomości. Zastanów się jak to zrobić. Zachęcam zapytać prowadzącego o podpowiedzi.

## Zadania domowe

Napisz program, który wygeneruje drzewko procesów wyglądające tak (oczywiście procesy nie będą się nazywały A, B, ...):

```raw
A \
  |\B \
  |   |\D
  |    \E
   \C \
      |\F
       \G
```

A jest rodzicem 2 procesów - B oraz C, a te są odpowiednio rodzicami D i E, oraz F i G.


## Zadanie dla ambitnych
Tego nie było na wykładzie ani na ćwiczeniach.
Zapoznaj się z funkcją [clone](http://linux.die.net/man/2/clone). Napisz program, który wykona na 2 procesach sumowanie kolejnych liczb. Pierwszy proces wykona sumowanie od 0 do 99, a drugi proces od 100 do 199. Niech obliczenia wykonają się równolegle, a proces rodzica zsumuje wyniki cząstkowe.
