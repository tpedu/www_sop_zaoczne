# Instalacja systemu Linux

Dzisiaj zainstalujemy sobie system Linux, dystrybucję Debian, na wirtualnych maszynach.

Są to zajęcia wstępne i rozgrzewkowe. Pierwotnie planowałem tu zajęcia z Bash, ale jednal lepiej, aby każda osoba miała doświadczenie w instalacji systemu.

## Kurs git-a

Bardzo dobry zestaw ćwiczeń z systemu kontroli wersii Git:

[learngitbranching](https://learngitbranching.js.org/)


## Zadanie dla chętnych

Zadanie polega na tym

Zainstaluj u siebie na wirtualnej maszynie (albo na hoście, ale wtedy niech to będzie komputer numer 2, bo należałoby pokazać na zajęciach jak działa) dystrybucję Gentoo. Chciałbym aby miała zainstalowane środowisko graficzne, sieć oraz przeglądarkę (firefox lub chrome).

Dla osób z grupy TP (w innych grupach zależy to od prowadzącego) za zrobienie tego można otrzymać 3 z kolokwium 1 (czyli tego dotyczącego bash-a). Czas do pierwszego kolokwium. W obecnych warunkach może się okazać że jest to do końca semestru, ale mam nadzieję (chociaż umysł mówi mi że ta nadzieja jest beznadzieją) że całe te epidemie skończą się wcześniej.
