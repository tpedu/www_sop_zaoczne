# SOP 2020 Lato, zaoczne

## Zasady zaliczenia

* (nieaktualne) Kolokwium na koniec
* Dwa kolokwia - bash i api systemowe
* Możliwośc zaliczenia pierwszego kolokwium na podstawie zadania dodatkowego z Gentoo
* Aktywność na zajęciach
* Egzamin

## Literatura

* A. Silberschatz, P.B.Galvin, G.Gagne: Podstawy Systemów Operacyjnych
* Wojciech Moch, Piotr Pilch: Jak działa Linux. Podręcznik administratora. Wydanie II
* Robert Love: Linux. Programowanie systemowe. Wydanie II
* Wykłady i ćwiczenia

## Wykłady

Będą one aktualizowane w trakcie przedmiotu

* [przykłady do SOP na Github - gniazda](https://github.com/pantadeusz/hello-c-network-sockets)
* [przykłady do SOP na Github](https://github.com/pantadeusz/sop)
* [sop_01.pdf](tex/01-os/sop_01.pdf)
* [sop_02.pdf](tex/02-bash/sop_02.pdf) [w2.tar.gz](data/w2.tar.gz)
* [sop_03.pdf](tex/03-bash-2/sop_03.pdf) [btcticker](data/btcticker)
* [sop_04.pdf](tex/04-awk-tar-inne/sop_04.pdf)
* [sop_05.pdf](tex/05-c/sop_05.pdf)
* [sop_06.pdf](tex/06-procesy-teoria/sop_06.pdf)
* [sop_07.pdf](tex/07-make/sop_07.pdf)
* [sop_08.pdf](tex/08-memory/sop_08.pdf)
* [sop_09.pdf](tex/09-vmem-fifo/sop_09.pdf)
* [sop_10.pdf](tex/10-shmem-sync/sop_10.pdf)
* [sop_11.pdf](tex/11-signal/sop_11.pdf)
* [sop_12.pdf](tex/12-sockets/sop_12.pdf)
* [sop_13.pdf](tex/13-sharedlibs/sop_13.pdf)
* [sop_14.pdf](tex/14-fs/sop_14.pdf)


## Plan przedmiotu

Na laboratoriach będziemy przerabiali następujące tematy:

1. Bash.
1. Bash.
1. Kompilacja, C.
1. Procesy.
1. Procesy.
1. Sygnały.
1. Biblioteki.
1. Koło praktyczne na komputerach.
