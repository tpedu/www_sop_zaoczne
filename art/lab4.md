# 04 Język C

2020-04-05

Literatura: 

* Brian W. Kernighan, Dennis M. Ritchie: Język ANSI C. Programowanie. Wydanie II

(Opcja) Prowadzący zrobi małe wprowadzenie do języka C.

* ANSI C, C99, ...
* komentarze
* zmienne (zobacz też [typy danych](https://en.wikipedia.org/wiki/C_data_types) )
  * tablice statyczne
  * tablice dynamiczne
  * wskaźniki
  * struktury
  * typy wyliczeniowe (enum)
  * unie (union)
* arytmetyka
* funkcje
  * wskaźniki na funkcje
* instrukcje warunkowe
* pętle
* wielowybór

## Przypomnienie

### Ważniejsze przykłady struktur

```c
union
{
    short int i;
    char j;
    struct
    {
        unsigned char lower;
        unsigned char higher;
    } s;
} u;
```

```c
struct s{
int a,b;
};
```

## 1 Zadania

1. Napisz program obliczający sumę liczb wprowadzonych przez użytkownika (ze standardowego wejścia) i wypisujący wynik na standardowe wyjście. Przydatne hasła to:
    * scanf
    * printf
    * operatory arytmetyczne
    * if
    * for albo while
1. Przerób ten program tak, aby wartość była zwracana jako kod zakończenia programu. Wyświetl tą wartość z linii komend. Przydatne komendy:
    * return
1. Napisz program wyświetlający tradycyjną choinkę o zadanej wysokości. Wysokość niech będzie podana jako parametr wywołania programu. Przydatne hasła to:
    * atoi
    * argc, argv
    * for

## 2 Zadania z IO

Komendy z rodziny ```*printf``` oraz ```*scanf``` służą do formatowanego wejścia i wyjścia - umdostępniają sporo możliwości. Przetrenujmy to.

### IO 1

Napisz program wczytujący z pliku tekstowego ciąg liczb. Format pliku jest taki:

1. liczba elementów ciągu (jako liczba naturalna)
1. Kolejne liczby - każda w nowej linii

Niech program wypisze je w odwrotnej kolejności. Program powinien przyjmować zero albo jeden argument:

* jeśli nie podano argumentu, to ma wczytywać ze standardowego wejścia
* jeśli podano argument, to jest to nazwa pliku do wczytania

Skorzystaj z ```fopen```, ```fclose```, ```fscanf```.

Zabezpiecz przed przepełnieniem bufora.

### IO 2

Rozwiń program z poprzedniego zadania o zapis wczytanej tablicy do pliku binarnego. (tym razem niech nazwa tego pliku będzie ustalona wewnątrz programu)

Rozwiń program tak, aby podanie argumentu:

```raw
--print
```

spowodowało wypisanie zawartości pliku binarnego.

Możesz skorzystać z hexedytora hte, który jest dostępny na pracowni, aby podejrzeć zawartość wygenerowanego pliku.

### IO 3

Rozwiń poprzednie zadanie o obsługę dowolnie wielkich tablic. Skorzystaj z ```malloc``` oraz ```free```.

## O wskaźnikach

Przy używaniu zmiennych:

* Gwiazdka oznacza pobranie wartości wskazywanej przez dany adres (wskaźnik).
* Znak & oznacza pobranie adresu danej zmiennej
Przy deklaracji zmiennych:
* Gwiazdka oznacza typ wskaźnikowy
* Można zadeklarować wskaźnik na funkcję, wtedy robi się to tak:
  * typ_zwracany (*nazwa_funkcji)(typy_argumentow,...);

```c
int z = 12;
int *y;
int **x;

y = &z;
x = &y;
// **x == 12; *y == 12; Nie wiadomo przed uruchomieniem jaka będzie wartość x oraz y. Dlaczego?
```

### Zadania

1. Napisz program z 3 funkcjami, każda funkcja niech będzie miała taką sygnaturkę:
   * int (*f)(int,int)
 Niech będą to funkcje: maksimum, minimum oraz suma.
1. Zapamiętaj wskaźniki do tych funkcji w tablicy trójelementowej
1. Niech program pobiera kolejno wartości:
1. liczba \\(z\\) od 0 do 2 oznaczająca indeks funkcji w tablicy
1. liczba elementów ciągu do pobrania (nazwijmy ją \\(n\\))
1. kolejno \\(n\\) liczb
1. Niech program przetwarza wczytaną tablicę w taki sposób, że:
    1. Niech \\(v\\) przyjmie wartość pierwszego elementu
    1. w pętli po wszystkich elementach od drugiego (indeks 1 w tablicy)
       * wykona funkcję o numerze \\(z\\) przekazując jako argumenty \\(v\\) oraz element o aktualnym indeksie. Niech wynik zostanie zapisany do \\(v\\)
    1. Wypisze wartość \\(v\\)

Czyli piszemy program który pozwoli na wykonanie albo sumy, albo znalezienia maksimum, albo znalezienia minimum z \\(n\\) liczb. Program nie będzie korzystał z instrukcji warunkowych (w pętli głównej) do wyboru jaka funkcja ma być wykonana.

Korzystając z wiedzy z poprzedniego zadania, napisz funkcję, która będzie przetwarzała tablicę za pomocą uniwersalnej funkcji podanej jako argument funkcji. Na przykład:

```c
int wynik = forAll(tablica, liczbaElementow, maksimum); // maksimum to nazwa funkcji porownojacej dwie wartosci
```

### Zadanie Domowe

Do przetrenowania. Zadanie jest dość ambitne, ale postaraj się zrobić to samodzielnie. Jest to bardzo dobre ćwiczenie na wskaźniki.

1. Napisz program implementujący listę jednostronnie wiązaną.
1. Napisz program implementujący listę dwustronnie wiązaną.
1. Napisz program liczący kolejne wartości ciągu Fibonacciego w wersji:
    * rekursywnej (rekurencyjnej)
    * iteracyjnej
