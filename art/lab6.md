# 06 Gniazda

* [getaddrinfo](http://man7.org/linux/man-pages/man3/getaddrinfo.3.html)
* [przykład z wykładu](https://github.com/pantadeusz/hello-c-network-sockets)

## Zadanie 1

Korzystając z przykładów (albo samodzielnie, lub na podstawie manuala) przygotuj programy - klient oraz serwer.

* Niech serwer będzie udostępniał wybrany plik.
* Niech klient będzie miał możliwość pobrania wybranego przez klienta pliku.
* Jako uproszczenie przyjmujemy że serwer nie dba o żadne zasady bezpieczeństwa.

Protokół komunikacji niech wygląda tak:

```raw
KLIENT                                  SERWER
długość_nazwy_pliku               >
nazwa_pliku[długość_nazwy_pliku]  >
                                  <   wielkość_pliku
                                  <   paczka_1[wielkość_pliku]
--zamknij połączenie--                --zamknij połączenie--
```

Serwer powinien działać w pętli.

## Zadanie 2

Niech serwer, w sytuacji, jeśli pliku nie udało się otworzyć, wysyła wielkość pliku jako ```-1```.

## Zadanie 3

Niech serwer zakończy w elegancki sposób pracę w momencie naciśnięcia przez użytkownika Ctrl+C. Chodzi o zamknięcie gniazda nasłuchującego.

## Zadanie 4

Niech serwer zapisuje logi do pliku - to znaczy informacje o tym kto się podłączył (jego IP oraz port) oraz o jaki plik poprosił.

## Zadanie domowe 1

Rozwiń/napisz serwer HTTP tak, aby nadawał się do udostępniania strony WWW. Serwer powinien obsługiwać:

* lista zabronionych adresów
* zapis logów zdarzeń (wraz z informacją kto się podłączył)

## Zadanie domowe 2

Dodatkowo niech serwer pozwala na tworzenie wirtualnych hostów. Niech serwer nie blokuje się w sytuacji kiedy jakiś klient przeciąga połączenie (aby nie blokowało ono innych połączeń). Zastosuj ```fork```.
